angular.module("weatherUI").controller("WeatherController", [
	"$scope",
	"geocode",
	"WeatherResource",
	function(
		$scope,
		geocode,
		WeatherResource
	) {
		$scope.cities = [];

		$scope.$on("addCity", function(event, city) {
			var duplicates = $scope.cities.filter(function(item) {
				return item.id === city.id;
			});
			if (!duplicates.length) {
				geocode(city.id)
					.then(function(response) {
						console.log(response);
						return WeatherResource.get(response).$promise;
					})
					.then(function(response) {
						console.log(response);
						city.temperature = response.temperature;
						$scope.cities.push(city);
					});
			}
		});
	}
]);
