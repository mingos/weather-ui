angular.module("weatherUI").factory("WeatherResource", [
	"config",
	"$resource",
	function(
		config,
		$resource
	) {
		return $resource(config.apiUrl + "weather", {}, {
			get: {method: "GET", isArray: false}
		});
	}
]);
