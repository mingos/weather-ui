/**
 * Transform a response from Google Places API into a standardised form.
 */
angular.module("weatherUI").factory("placesResponse", [
	function() {
		return function placesResponse(predictions) {
			return predictions
				.filter(function(place) {
					return place.types && place.types.indexOf("locality") > -1;
				})
				.map(function(place) {
					return {
						city: place.description,
						id: place.place_id
					};
				});
		}
	}
]);
