/**
 * Find places matching a query term and return them as a standardised response.
 */
angular.module("weatherUI").factory("placesSearch", [
	"$q",
	"placesResponse",
	function(
		$q,
		placesResponse
	) {
		var autocompleteService = new google.maps.places.AutocompleteService();

		return function placesSearch(query) {
			var request = {
				input: query,
				types: "(cities)",
				language: "en"
			};

			var deferred = $q.defer();
			autocompleteService.getQueryPredictions(request, function(predictions, status) {
				if (status === google.maps.places.PlacesServiceStatus.OK) {
					deferred.resolve(placesResponse(predictions));
				} else {
					deferred.reject();
				}
			});

			return deferred.promise;
		}
	}
]);
