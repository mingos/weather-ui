/**
 * Get geocoding information about a place based on a PlaceID
 */
angular.module("weatherUI").factory("geocode", [
	"$q",
	"geocodeResponse",
	function(
		$q,
		geocodeResponse
	) {
		var geocoder = new google.maps.Geocoder();

		return function(placeId) {
			var deferred = $q.defer();
			var request = {
				placeId: placeId,
				language: "en"
			};
			geocoder.geocode(request, function(result, status) {
				if (status === google.maps.GeocoderStatus.OK) {
					deferred.resolve(geocodeResponse(result));
				} else {
					deferred.reject();
				}
			});
			return deferred.promise;
		};
	}
]);
