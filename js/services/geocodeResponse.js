/**
 * Transform a result set from Google Maps API Geocoder into an object
 * that can be used to retrieve weather from the backend.
 */
angular.module("weatherUI").factory("geocodeResponse", [
	function() {
		return function(results) {
			var result = results[0];

			var nameComponent = result.address_components
				.filter(function(component) {
					return component.types.indexOf("locality") > -1;
				})[0];

			var countryComponent = result.address_components
				.filter(function(component) {
					return component.types.indexOf("country") > -1;
				})[0];

			return {
				city: nameComponent.long_name,
				country: countryComponent.long_name,
				countryCode: countryComponent.short_name,
				latitude: result.geometry.location.lat(),
				longitude: result.geometry.location.lng()
			};
		}
	}
]);
