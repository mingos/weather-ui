angular.module("weatherUI").directive("cityChooser", [
	"placesSearch",
	function(
		placesSearch
	) {
		return {
			restrict: "E",
			templateUrl: "js/directives/cityChooser.html",
			link: function(scope) {
				scope.query = "";
				scope.results = [];

				scope.find = function(query) {
					placesSearch(query).then(function(results) {
						scope.results = results;
					});
				};

				scope.add = function(result) {
					scope.$emit("addCity", result);
				};
			}
		};
	}
]);
