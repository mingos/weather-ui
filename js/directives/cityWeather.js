angular.module("weatherUI").directive("cityWeather", [
	function() {
		return {
			restrict: "E",
			templateUrl: "js/directives/cityWeather.html",
			scope: {
				cities: "="
			},
			link: function(scope) {
				scope.remove = function(city) {
					var index = scope.cities.indexOf(city);
					scope.cities.splice(index, 1);
				}
			}
		};
	}
]);
