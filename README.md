# Weather UI

A simple UI communicating with a weather API.

## Installation

Dependencies required to run the application can be installed as follows:

```
npm install
```

Note that this command assumes that you have **bower** installed globally. If you do not, then first install it:

```
npm install -g bower
```

## Building

The build process ican be triggered with a single command:

```
npm run build
```

Note that it requires that you have **gulp** installed globally. If you do not, please install it first:

```
npm install -g gulp
```

## Testing

Unit tests can be launched with the command:

```
npm test
```

## Running the server

Since the application makes cross-domain requests, it is not sufficient to open the **index.html** file in a browser.
A running Connect server can be used to overcome this issue:

```
npm start
```

The server should start listening on port 8000 unless a different port is specified in the `PORT` env variable:

```
PORT=8001 npm sart
```
