var gulp = require("gulp");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
var template = require("gulp-template");

var files = [
	"bower_components/angular/angular.js",
	"bower_components/angular-resource/angular-resource.js",
	"js/**/*.js"
];
var templateVars = {
	config: require(__dirname + "/../config/config.json")
};

gulp.task("js:dev", function() {
	gulp.src(files)
		.pipe(concat("scripts.js"))
		.pipe(template(templateVars))
		.pipe(gulp.dest("."));
});

gulp.task("js:dist", function() {
	gulp.src(files)
		.pipe(concat("scripts.js"))
		.pipe(template(templateVars))
		.pipe(uglify())
		.pipe(gulp.dest("."));
});
