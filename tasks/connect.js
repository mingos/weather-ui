var gulp = require("gulp");
var connect = require("gulp-connect");

gulp.task("connect", function() {
	connect.server({
		port: process.env.PORT || 8000,
		root: "."
	});
});
