var gulp = require("gulp");
var less = require("gulp-less");
var autoprefixer = require("gulp-autoprefixer");
var nano = require("gulp-cssnano");
var rename = require("gulp-rename");

gulp.task("css:dev", function() {
	gulp.src("less/index.less")
		.pipe(less())
		.pipe(rename("style.css"))
		.pipe(gulp.dest("."));
});

gulp.task("css:dist", function() {
	gulp.src("less/index.less")
		.pipe(less())
		.pipe(autoprefixer())
		.pipe(nano())
		.pipe(rename("style.css"))
		.pipe(gulp.dest("."));
});
