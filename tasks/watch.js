var gulp = require("gulp");

gulp.task("watch", function() {
	gulp.watch("less/**/*.less", ["css:dev"]);
	gulp.watch("js/**/*.js", ["js:dev", "test"]);
	gulp.watch("spec/**/*.js", ["test"]);
});
