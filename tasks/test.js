var gulp = require("gulp");
var Karma = require("karma").Server;

gulp.task("test", function(done) {
	new Karma({
		configFile: __dirname + "/../karma.conf.js"
	}, function() {
		done();
	}).start();
});
