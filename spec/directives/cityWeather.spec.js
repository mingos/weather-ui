describe("cityWeather directive", function() {
	var cities = [
		{city: "Gdańsk, Polska", id: "ChIJb_rUFBxz_UYRjb63Y_H7uZs", temperature: 3.24},
		{city: "London, Wielka Brytania", id: "ChIJdd4hrwug2EcRmSrV3Vo6llI", temperature: 8.65},
		{city: "Paris, Francja", id: "ChIJD7fiBh9u5kcRYJSMaMOCCwQ", temperature: 10.12}
	];

	beforeEach(module("weatherUI"));
	beforeEach(module("weatherUIHTML"));

	var element, scope;
	beforeEach(inject(function($rootScope, $compile) {
		var html = '<city-weather cities="cities"></city-weather>';
		$rootScope.cities = [];
		scope = $rootScope.$new();
		element = $compile(html)(scope);
		scope.$digest();
	}));

	it("displays an empty list", function() {
		// given
		// when
		// then
		expect(element.find("p").length).toBe(1);
		expect(element.find("p")[0].textContent).toContain("No cities on the list so far.");
	});

	it("displays cities with temperatures", function() {
		// given
		var isolateScope = element.isolateScope();

		// when
		isolateScope.cities = cities;
		isolateScope.$digest();

		// then
		expect(element.find("p").length).toBe(0);
		expect(element[0].querySelectorAll(".city-weather__list").length).toBe(1);

		var items = element[0].querySelectorAll(".city-weather__list-item");
		expect(items.length).toBe(3);
		expect(items[0].textContent).toContain("Gdańsk, Polska");
		expect(items[0].textContent).toContain("3.24");
		expect(items[1].textContent).toContain("London, Wielka Brytania");
		expect(items[1].textContent).toContain("8.65");
		expect(items[2].textContent).toContain("Paris, Francja");
		expect(items[2].textContent).toContain("10.12");
	});

	it("removes cities from the list", function() {
		// given
		var isolateScope = element.isolateScope();

		// when
		isolateScope.cities = cities;
		isolateScope.$digest();

		element.find("button").eq(1).triggerHandler("click");

		// then
		var items = element[0].querySelectorAll(".city-weather__list-item");
		expect(items.length).toBe(2);
		expect(items[0].textContent).toContain("Gdańsk, Polska");
		expect(items[1].textContent).toContain("Paris, Francja");
	});
});
