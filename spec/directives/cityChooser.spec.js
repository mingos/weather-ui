describe("cityChooser directive", function() {
	var response = [
		{city: "Paris, Francja", id: "691b237b0322f28988f3ce03e321ff72a12167fd"},
		{city: "Paris, Teksas, Stany Zjednoczone", id: "518e47f3d7f39277eb3bc895cb84419c2b43b5ac"},
		{city: "Paris, Tennessee, Stany Zjednoczone", id: "9093091daae9a06a3695203bd8e68a2defef674b"}
	];

	beforeEach(module("weatherUI"));
	beforeEach(module("weatherUIHTML"));
	beforeEach(module(function($provide) {
		$provide.factory("placesSearch", function($q) {
			return function() {
				var deferred = $q.defer();
				deferred.resolve(response);
				return deferred.promise;
			};
		});
	}));

	var element, scope, rootScope;
	beforeEach(inject(function($rootScope, $compile) {
		var html = '<city-chooser></city-chooser>';
		rootScope = $rootScope;
		scope = $rootScope.$new();
		element = $compile(html)(scope);
		scope.$digest();
	}));

	it("displays an empty input", function() {
		// given
		// when
		// then
		expect(element.find("input").length).toBe(1);
		expect(element.find("ul").length).toBe(0);
	});

	it("displays a cities list on search", function() {
		// given
		var model = element.find("input").controller("ngModel");

		// when
		model.$setViewValue("Paris");
		element.find("input").triggerHandler("blur"); // instant model update on blur

		// then
		expect(element.find("input").length).toBe(1);
		expect(element[0].querySelectorAll(".city-chooser__list").length).toBe(1);
		var items = element[0].querySelectorAll(".city-chooser__list-item");
		expect(items.length).toBe(3);
		expect(items[0].textContent).toContain("Paris, Francja");
		expect(items[1].textContent).toContain("Paris, Teksas, Stany Zjednoczone");
		expect(items[2].textContent).toContain("Paris, Tennessee, Stany Zjednoczone");
	});

	it("emits the clicked city", function() {
		// given
		var model = element.find("input").controller("ngModel");
		var callback = jasmine.createSpy("callback", function(input) {});
		rootScope.$on("addCity", function(event, arg) {
			callback(arg);
		});

		// when
		model.$setViewValue("Paris");
		element.find("input").triggerHandler("blur");

		element.find("button").eq(0).triggerHandler("click");

		// then
		expect(callback).toHaveBeenCalledWith(response[0]);
	});
});
