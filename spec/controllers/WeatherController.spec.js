describe("WeatherController", function() {
	beforeEach(module("weatherUI"));

	var controller, scope;
	beforeEach(inject(function($controller, $rootScope, $q) {
		var geocode = function() {
			var deferred = $q.defer();
			deferred.resolve({
				city: "Foo",
				country: "Bar",
				countryCode: "Ba",
				latitude: 123.456,
				longitude: 12.3456
			});
			return deferred.promise;
		};
		var WeatherResource = {
			get: function() {
				var deferred = $q.defer();
				deferred.resolve({
					temperature: 12.34
				});
				return {
					$promise: deferred.promise
				};
			}
		};
		scope = $rootScope.$new();
		controller = $controller("WeatherController", {
			$scope: scope,
			geocode: geocode,
			WeatherResource: WeatherResource
		});
		scope.$digest();
	}));

	var city1 = {city: "Gdańsk, Polska", id: "ChIJb_rUFBxz_UYRjb63Y_H7uZs"};
	var city2 = {city: "London, Wielka Brytania", id: "ChIJdd4hrwug2EcRmSrV3Vo6llI"};

	var city1WithTemp = angular.extend({temperature: 12.34}, city1);
	var city2WithTemp = angular.extend({temperature: 12.34}, city2);

	it("starts with an empty list of cities", function() {
		// given
		// when
		// then
		expect(scope.cities).toEqual([]);
	});

	it("adds cities to the list", function() {
		// given
		var childScope = scope.$new();

		// when
		childScope.$emit("addCity", city1);
		scope.$digest();

		childScope.$emit("addCity", city2);
		scope.$digest();

		// then
		expect(scope.cities).toEqual([city1WithTemp, city2WithTemp]);
	});

	it("prevents duplicates on the cities list", function() {
		// given
		var childScope = scope.$new();

		// when
		childScope.$emit("addCity", city1);
		scope.$digest();

		childScope.$emit("addCity", city1);
		scope.$digest();

		// then
		expect(scope.cities).toEqual([city1WithTemp]);
	});
});
