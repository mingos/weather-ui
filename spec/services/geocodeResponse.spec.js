describe("geocodeResponse service", function() {
	beforeEach(module("weatherUI"));

	var service;
	beforeEach(inject(function(geocodeResponse) {
		service = geocodeResponse;
	}));

	it("transforms a Google Geocoder response", function() {
		// given
		var results = [
			{
				"address_components": [
					{
						"long_name": "Gdańsk",
						"short_name": "Gdańsk",
						"types": ["locality", "political"]
					},
					{
						"long_name": "Gdańsk",
						"short_name": "Gdańsk",
						"types": ["administrative_area_level_3", "political"]
					},
					{
						"long_name": "Gdańsk",
						"short_name": "Gdańsk",
						"types": ["administrative_area_level_2", "political"]
					},
					{
						"long_name": "pomorskie",
						"short_name": "pomorskie",
						"types": ["administrative_area_level_1", "political"]
					},
					{
						"long_name": "Polska",
						"short_name": "PL",
						"types": ["country", "political"]
					}
				],
				"formatted_address": "Gdańsk, Polska",
				"geometry": {
					"bounds": {
						"northeast": {
							"lat": 54.44721879999999,
							"lng": 18.9482841
						},
						"southwest": {
							"lat": 54.27495589999999,
							"lng": 18.4287748
						}
					},
					"location": { // raw response is transformed by Google's Geocoder service
						"lat": function() { return 54.35202520000001; },
						"lng": function() { return 18.6466384; }
					},
					"location_type": "APPROXIMATE",
					"viewport": {
						"northeast": {
							"lat": 54.44721879999999,
							"lng": 18.9482841
						},
						"southwest": {
							"lat": 54.27495589999999,
							"lng": 18.4287748
						}
					}
				},
				"place_id": "ChIJb_rUFBxz_UYRjb63Y_H7uZs",
				"types": ["locality", "political"]
			}
		];

		// when
		var transformedResponse = service(results);

		// then
		expect(transformedResponse).toEqual({
			city: "Gdańsk",
			country: "Polska",
			countryCode: "PL",
			latitude: 54.35202520000001,
			longitude: 18.6466384
		});
	});
});
