describe("placesResponse service", function() {
	beforeEach(module("weatherUI"));

	var service;
	beforeEach(inject(function(placesResponse) {
		service = placesResponse;
	}));

	it("transforms a Google Places response", function() {
		// given
		var predictions = [
			{
				"description": "Paris, Francja",
				"id": "691b237b0322f28988f3ce03e321ff72a12167fd",
				"matched_substrings": [
					{
						"length": 5,
						"offset": 0
					}
				],
				"place_id": "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
				"reference": "CjQmAAAAHEgYzdNLZK40SG1ms2ENfVj-qK4z2s2feYfzKAAbt-m8e8QKS3vt_IYv4gdiCyN_EhC1JWN8Z1bGdx0omEVmSiwJGhTLzqz0o0Zds6OCWlHhVPBoe2pN5Q",
				"terms": [
					{
						"offset": 0,
						"value": "Paris"
					},
					{
						"offset": 7,
						"value": "Francja"
					}
				],
				"types": ["locality", "political", "geocode"]
			},
			{
				"description": "Paris, Teksas, Stany Zjednoczone",
				"id": "518e47f3d7f39277eb3bc895cb84419c2b43b5ac",
				"matched_substrings": [
					{
						"length": 5,
						"offset": 0
					}
				],
				"place_id": "ChIJmysnFgZYSoYRSfPTL2YJuck",
				"reference": "CkQ4AAAAKbx15nlEXZw29HnTsvkMvylZQmL9V_xnY1VOgRp7Xz8KEGRb9vocPy6UHBAZyftfZnG91SCELRD3M9_J04viKBIQv2eNEg9TolMD2R6ZHKBtrhoUfKgfxdnim0ad7l69JgIyx47qfsQ",
				"terms": [
					{
						"offset": 0,
						"value": "Paris"
					},
					{
						"offset": 7,
						"value": "Teksas"
					},
					{
						"offset": 15,
						"value": "Stany Zjednoczone"
					}
				],
				"types": ["locality", "political", "geocode"]
			},
			{
				"description": "Pariser Platz, Berlin, Niemcy",
				"id": "0e85a595042c2602b2e86a4a9f4aaa4fe07d0bab",
				"matched_substrings": [
					{
						"length": 5,
						"offset": 0
					}
				],
				"place_id": "Eh1QYXJpc2VyIFBsYXR6LCBCZXJsaW4sIE5pZW1jeQ",
				"reference": "CjQhAAAAa8lTgDLPcOENDdmb0VwisHkNubWkgR6oT7L0w5kbqRyjwSntv1-2IcRbl8O3jpyVEhCpose2dB1IDLrP0q8zTGzsGhToyz0SYIAfG-WQ30j9luyHzCfaCA",
				"terms": [
					{
						"offset": 0,
						"value": "Pariser Platz"
					},
					{
						"offset": 15,
						"value": "Berlin"
					},
					{
						"offset": 23,
						"value": "Niemcy"
					}
				],
				"types": ["route", "geocode"]
			},
			{
				"description": "Paris Las Vegas, South Las Vegas Boulevard, Las Vegas, Nevada, Stany Zjednoczone",
				"id": "4cfcf4df90bdddceff3320abb8875ef519cc719e",
				"matched_substrings": [
					{
						"length": 5,
						"offset": 0
					}
				],
				"place_id": "ChIJAQAAAGzDyIARAVwe_ga0REU",
				"reference": "CmReAAAAh8yA_6XOpMzoNkGSHbju_zpd0v0TNGmcGmNL00hkJvILMQfAO6K464oNiM_N66iapQDpWdszK6KpZSxy9BApya5JSWivwOraEeIEJOt0HHg8Se5JhYEUCnZN-9IzwtQbEhAhVQr1v8li12kmtDd8_96fGhTgXSAVwl_Grgu92jnQmW_D0T0qRQ",
				"terms": [
					{
						"offset": 0,
						"value": "Paris Las Vegas"
					},
					{
						"offset": 17,
						"value": "South Las Vegas Boulevard"
					},
					{
						"offset": 44,
						"value": "Las Vegas"
					},
					{
						"offset": 55,
						"value": "Nevada"
					},
					{
						"offset": 63,
						"value": "Stany Zjednoczone"
					}
				],
				"types": ["establishment"]
			},
			{
				"description": "Paris, Tennessee, Stany Zjednoczone",
				"id": "9093091daae9a06a3695203bd8e68a2defef674b",
				"matched_substrings": [
					{
						"length": 5,
						"offset": 0
					}
				],
				"place_id": "ChIJ4zHP-Sije4gRBDEsVxunOWg",
				"reference": "CkQ7AAAA1kwv0cqMuPWxzOsL8tGnE8p-zNkD2vRodoh99RIHoF7j6FDdh7GEWOtgTdl2Bo8eLwYq40wS0u9Z_T9K6hCDwhIQwA1NnVaifn-kugoVfcI_8xoU10yOXXmVBJkd06-v10rhYu_Ju78",
				"terms": [
					{
						"offset": 0,
						"value": "Paris"
					},
					{
						"offset": 7,
						"value": "Tennessee"
					},
					{
						"offset": 18,
						"value": "Stany Zjednoczone"
					}
				],
				"types": ["locality", "political", "geocode"]
			}
		];

		// when
		var transformedResponse = service(predictions);

		// then
		expect(transformedResponse).toEqual([
			{city: "Paris, Francja", id: "ChIJD7fiBh9u5kcRYJSMaMOCCwQ"},
			{city: "Paris, Teksas, Stany Zjednoczone", id: "ChIJmysnFgZYSoYRSfPTL2YJuck"},
			{city: "Paris, Tennessee, Stany Zjednoczone", id: "ChIJ4zHP-Sije4gRBDEsVxunOWg"}
		]);
	});
});
