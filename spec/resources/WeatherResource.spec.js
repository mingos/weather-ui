describe("weather resource", function() {
	beforeEach(module("weatherUI"));
	beforeEach(module(function($provide) {
		$provide.constant("config", {
			apiUrl: "http://example.com/"
		});
	}));

	var resource, http;
	beforeEach(inject(function(WeatherResource, $httpBackend) {
		http = $httpBackend;
		resource = WeatherResource;
	}));
	afterEach(function() {
		http.verifyNoOutstandingRequest();
		http.verifyNoOutstandingExpectation();
	});

	it("fetches the temperature for a city", function() {
		// given
		var params = {
			city: "Gdańsk",
			country: "Poland",
			countryCode: "PL",
			latitude: 54.3610063,
			longitude: 18.5484488
		};
		http
			.expectGET("http://example.com/weather?city=Gda%C5%84sk&country=Poland&countryCode=PL&latitude=54.3610063&longitude=18.5484488")
			.respond({temperature: 3.14});


		// when
		var request = resource.get(params);
		http.flush();

		// then
		request.$promise.then(function(response) {
			expect(response instanceof resource).toBe(true);
			expect(response.temperature).toBe(3.14);
		});
	});
});
