// Karma configuration
// Generated on Sat Feb 06 2016 00:37:27 GMT+0100 (CET)

module.exports = function (config) {
	config.set({
		basePath: "",
		frameworks: ["jasmine"],
		files: [
			"bower_components/angular/angular.js",
			"bower_components/angular-resource/angular-resource.js",
			"bower_components/angular-mocks/angular-mocks.js",

			"js/app.js",
			"js/constants/*.js",
			"js/services/*.js",
			"js/resources/*.js",
			"js/controllers/*.js",
			"js/directives/*.js",

			"js/**/*.html",

			"spec/**/*.spec.js"
		],
		exclude: [],
		preprocessors: {
			"js/**/*.html": ["ng-html2js"]
		},
		reporters: ["story"],
		port: 9876,
		colors: true,
		logLevel: config.LOG_INFO,
		autoWatch: false,
		browsers: ["PhantomJS2"],
		singleRun: true,
		concurrency: Infinity,
		ngHtml2JsPreprocessor: {
			moduleName: "weatherUIHTML"
		}
	})
};
